;;; publish.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Jake Shilling
;;
;; Author: Jake Shilling <shilling.jake@gmail.com>
;; Maintainer: Jake Shilling <shilling.jake@gmail.com>
;; Created: May 16, 2022
;; Modified: May 16, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/j-shilling/publish
;; Package-Requires: ((emacs "24.4"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'ox-publish)

(setq org-publish-project-alist
      `(("site-org"
         :base-directory "."
         :base-extension "org"
         :recursive t
         :publishing-function (org-html-publish-to-html)
         :publishing-directory "./public"
         :with-title nil)
        ("site-static"
         :base-directory "."
         :exclude "public/"
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
         :publishing-directory "./public"
         :publishing-function (org-publish-attachment)
         :recursive t)
        ("site"
         :components ("site-org" "site-static"))))

(provide 'publish)
;;; publish.el ends here
